const host = 'http://10.162.108.177:8080'; 
const button = document.querySelector('.loginButton'); 
let dadosEx = ""; 

const logar = function(){ 
    let body = { 
        email: document.querySelector('input[name="login"]').value, 
        password: document.querySelector('input[name="pass"').value
    }
    post(body); 
}

const post = function(body){ 
    fetch('https://reqres.in/api/login' + 'login', {method: 'POST', headers: {'Content-type': 'application/json'}, body: JSON.stringify(body)})
    .then(function(response){
        return response.json()
    })
    .then(function teste(dados){
        console.log(dados.email); 
        window.location = 'index.html';
        localStorage.setItem('token', dados);
    })
}
button.onclick = logar; 
